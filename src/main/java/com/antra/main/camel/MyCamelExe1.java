package com.antra.main.camel;

import java.util.StringTokenizer;

import org.apache.camel.Message;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.antra.main.model.Employee;
import com.google.gson.Gson;

//@Component
public class MyCamelExe1 extends RouteBuilder {
    
    @Autowired
    private Employee employee;

    @Override
    public void configure() throws Exception {

	/**
	 * from() method indicates source location
	 */
	from("file:C:\\Users\\Anugna\\Documents\\sourcecamel")

		/**
		 * used to process the data
		 */
		.process((ex) -> {
		    Message input = ex.getIn();
		    String body = input.getBody(String.class);
		    StringTokenizer tokenizer = new StringTokenizer(body, ",");

		    /*StringBuilder builder = new StringBuilder();
		    String emp[] = { "id", "name", "role", "salary", "location" };
		    int i = 0;
		    builder.append("{");
		    while (tokenizer.hasMoreTokens()) {
			builder.append("\n" + emp[i] + " : " + tokenizer.nextToken());
			i++;
		    }
		    builder.append("\n}");*/
//		    String id = tokenizer.nextToken();
//		    String name = tokenizer.nextToken();
//		    String role = tokenizer.nextToken();
//		    String salary = tokenizer.nextToken();
		    
		    employee.setId(tokenizer.nextToken().trim());
		    employee.setName(tokenizer.nextToken().trim());
		    employee.setRole(tokenizer.nextToken().trim());
		    employee.setSalary(tokenizer.nextToken().trim());
		    employee.setLocation(tokenizer.nextToken().trim());
		    
		    String json = new Gson().toJson(employee);

		    Message output = ex.getMessage();
		    output.setBody(json);
//		    output.setBody(builder);
//			    "{\nid : " + id + "\nname : " + name + "\nrole : " + role + "\nsalary : " + salary + "\n}");
		})

		/**
		 * to() method indicates destination, the files of source will be moved to the
		 * destination
		 */
		.to("file:C:\\Users\\Anugna\\Documents\\desticamel");
    }
}