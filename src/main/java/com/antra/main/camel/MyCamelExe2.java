package com.antra.main.camel;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class MyCamelExe2 extends RouteBuilder {

    @Override
    public void configure() throws Exception {
//	moveAllFiles();
//	moveSpecificFile("student");
	moveSpecificBodyFile("java");
    }

    public void moveAllFiles() {
	from("file:C:\\Users\\Anugna\\Documents\\sourcecamel").to("file:C:\\Users\\Anugna\\Documents\\desticamel");
    }

    public void moveSpecificFile(String type) {
	from("file:C:\\Users\\Anugna\\Documents\\sourcecamel").filter(header(Exchange.FILE_NAME).startsWith(type))
		.to("file:C:\\Users\\Anugna\\Documents\\desticamel");
    }

    public void moveSpecificBodyFile(String body) {
	from("file:C:\\Users\\Anugna\\Documents\\sourcecamel?noop=true").filter(body().convertToString().contains(body))
		.to("file:C:\\Users\\Anugna\\Documents\\desticamel");
    }
}