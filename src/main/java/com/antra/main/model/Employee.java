package com.antra.main.model;

import org.springframework.stereotype.Component;

@Component
public class Employee {

    private String id;
    private String name;
    private String role;
    private String salary;
    private String location;

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getRole() {
	return role;
    }

    public void setRole(String role) {
	this.role = role;
    }

    public String getSalary() {
	return salary;
    }

    public void setSalary(String salary) {
	this.salary = salary;
    }

    public String getLocation() {
	return location;
    }

    public void setLocation(String location) {
	this.location = location;
    }
}